##### FSE16-BidText-Artifacts

This the the repository of all config files, scripts, JARs and some test apps
used in the FSE16 paper BidText.

> Notice: The 100 apps were stored with Git LFS but it seems they are not there anymore. I tried to recover the apps from my disk, but unfortunately SEVEN of them were lost.

    com.gizapps.aircombat-10.apk
    com.mos-22.apk
    com.ssdevapp.yoga-5.apk
    com.vetalgames.wildcops-9.apk
    com.wallpapers.aircraft.airplane.planes.images.backgrounds-18.apk
    com.wAmershamWycombeCollege-1424126411.apk
    com.xiaomistudio.tools.finalmail-13.apk

> The remaining 93 apps can be downloaded here: https://drive.google.com/file/d/1jIin_ItQb5u-9uwpBtU5T8IDyI6qXeBZ/view  or https://gitlab.com/hjjandy/fse16-bidtext-100apks (via LFS)

> The LFS-stored stanford-parser-3.4.1-models.jar is also unavailable. Please download it somewhere else.

The pre-configured VM can be found here: https://github.com/hjjandy/FSE16-BidText-Artifacts-VM. (Not available currently. I will update the link when available.)

##### How to download the repo

Some files have a larger size than 100MB. We leverage 
Git LFS (https://git-lfs.github.com/) to store the files. 
The users should install Git LFS before cloning the repository. 
With Git LFS installed, the users can download the large files 
via "git lfs clone", "git lfs pull".

Note that BidText-Bin/lib/stanford-parser-3.4.1-models.jar is stored 
with Git LFS. The test set of 100 apps under folder BidText-TestApps/Others
is also stored with Git LFS. The users MUST get a full local copy of 
the jar file to run BidText. For the test set, it is optional. 
The users can use any other arbitary Android apps to test BidText.


##### Prerequisite

Java 7 or 8 with amd64 architecture is required to run BidText on either 
Windows or unix-like operating systems.

##### How to run BidText

Please read FSE16-Artifacts-Eval-README for more details.

##### Where is the source code

The source code is not included. But it is publically available at: https://bitbucket.org/hjjandy/toydroid.bidtext


