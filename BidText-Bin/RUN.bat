if "%DEBUG%" == "" @echo off

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set BidText_TEST_HOME=%DIRNAME%..

@rem generate Config.properties
call set UNIX_STYLE_DIRNAME=%%DIRNAME:\=/%%
echo ANDROID_JAR = %UNIX_STYLE_DIRNAME%Android-platform-21/android.jar> "%DIRNAME%dat\Config.properties"
echo EXCLUSION_FILE = dat/AndroidRegressionExclusions.txt>> "%DIRNAME%dat\Config.properties"


for /f "delims=" %%x in (%DIRNAME%bidtext.prop) do (set "%%x")

:set_Java_Xmx
if [%JAVA_MAX_HEAP%]==[] goto set_BidText_Timeout
set JAVA_OPTS="-Xmx%JAVA_MAX_HEAP%"

:set_BidText_Timeout
if [%BIDTEXT_TIMEOUT%]==[] goto execute_cmd
set TOYDROID_BIDTEXT_OPTS="-DBidText.Timeout=%BIDTEXT_TIMEOUT%"

:execute_cmd

del /F /Q *.txt LOG.log

if exist "%1" (goto do_job) else (goto run_fail)

:do_job

set RESULT_DIR=%1.bidtext

if exist "%RESULT_DIR%" rmdir /S /Q "%RESULT_DIR%"
mkdir "%RESULT_DIR%"
call "%DIRNAME%bin\toydroid.bidtext.bat" "%1"

if exist *.txt (
	move /Y *.txt "%RESULT_DIR%"
	move /Y LOG.log "%RESULT_DIR%"
) else (
	del /F /Q LOG.log 
	rmdir /S /Q "%RESULT_DIR%"
)
goto run_end
	
:run_fail
echo "Invalid APP path: " %1

:run_end
if "%OS%"=="Windows_NT" endlocal
