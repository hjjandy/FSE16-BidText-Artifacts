@if "%DEBUG%" == "" @echo off

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set BidText_TEST_HOME=%DIRNAME%..

for /F %%f in (%DIRNAME%Eval-100.list) do (
    call %DIRNAME%\\RUN.bat "%BidText_TEST_HOME%\BidText-TestApps\Others\%%f"
)

if "%OS%"=="Windows_NT" endlocal