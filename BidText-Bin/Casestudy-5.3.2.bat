@if "%DEBUG%" == "" @echo off

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set BidText_TEST_HOME=%DIRNAME%..

call %DIRNAME%\\RUN.bat "%BidText_TEST_HOME%\BidText-TestApps\CaseStudy-5.3.2\com.pro.find.differences-5.apk"

if "%OS%"=="Windows_NT" endlocal